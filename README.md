
This is a simple form for account creation. It was created in an attempt to [have a custom login form for Pleroma's Gitlab so that we can have a captcha system to combat spam](https://git.pleroma.social/pleroma/pleroma-meta/-/issues/54#note_89436).

Currently it's a simple proof-of-concept. The idea is that people can get a simple form to provide needed data, including the answer to a [kocaptcha](https://github.com/koto-bank/kocaptcha). When the captcha is wrong, they get a message saying that. When the captcha is correct, a command is run in Bash.

The idea is that this command is replaced with a command that can make a new gitlab user.

## Installation

```sh
# Get the source
git clone https://codeberg.org/ilja/simple_register_form
cd simple_register_form

# Install Python dependencies
python3 -m venv venv
source venv/bin/activate
pip3 install Flask
pip3 install requests

# get config file
cp config.py_example config.py
```

Then to run it

```sh
# Run for development
source venv/bin/activate

export FLASK_APP=simple_register_form
export FLASK_ENV=development
export FLASK_RUN_HOST=127.0.0.1
export FLASK_RUN_PORT=5000

flask run
```

```sh
# Run for production
source venv/bin/activate
export FLASK_RUN_HOST=127.0.0.1
export FLASK_RUN_PORT=5000

python3 ./simple_register_form/__init__.py
```

## Configuration

You can change settings in config.py.

## Contributing

Feel free to open issues or PR's. However, the form is supposed to remain KISS.

Things I consider acceptable:
* Multiple options for floss and self hostable captcha systems to use
* Custom templates

Things I consider out of scope:
* Proprietary and otherwise unethical captcha systems
* Heavy customisations through settings
    * Overriding the templates with custom ones is OK, adding new inputs and stuff through settings is not.
* Settings for listening on different IP or PORT
    * This can be done with environment variables during start up

## License

Consider this released under an AGPL-v3 or later license
