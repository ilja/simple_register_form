from flask import Flask, render_template, request

import config
import hashlib
import os
import requests
import re
import subprocess

app = Flask(__name__)

# When we do a call to the kocaptcha provider, we get a token, a hash and a path for the image.
# When someone submits their form, we can use the token to see what's the corresponding hash. For that reason we need to keep the token and hash in cache.
# Note that we currently only throw the tokens away on restart. This is something that can be improved.
#
# The dict is of the form {token: {'md5': md5}}
#   e.g. {'065b02c36c756e3f63dd8642c8deb682': {'md5': '3665a926a883cbc9a69390e515ca7bc7'}}
# One idea is that we could add a timestamp and clear up 5 minutes old tokens when a new one is added
#   e.g. {'065b02c36c756e3f63dd8642c8deb682': {'md5': '3665a926a883cbc9a69390e515ca7bc7', 'created_at': datetime.now()}}
tokens = {}

captcha_base_url = config.captcha_base_url
command = config.command
command_arguments = config.command_arguments

@app.route('/')
def index():
  captcha_info = _get_and_store_captcha_info()
  return render_template('index.html', params={'token': captcha_info['token'], 'captcha_image': captcha_base_url + captcha_info['url']})

@app.route('/create_account', methods = ['POST'])
def create_account():
  data = request.form
  if _check_token(data["token"], data['captcha']):
    execute_return = subprocess.run(
      [command, _replace_with_form_values(command_arguments, data)], 
      capture_output=True
    )
    
    return render_template('create_account.html', params={'stdout': execute_return.stdout.decode('ascii'), 'stderr': execute_return.stderr.decode('ascii')})
  else:
    return render_template('create_account.html', params={'stderr': "Captcha didn't match"})

def _get_and_store_captcha_info():
  x = requests.get(captcha_base_url + '/new').json()
  tokens[x['token']] = {'md5': x['md5']}
  return x

def _check_token(token, captcha):
  md5 = hashlib.md5(captcha.encode('ascii')).hexdigest()
  return tokens[token]['md5'] == md5

def _replace_with_form_values(command_arguments, form_data):
  for key in form_data:
    command_arguments = command_arguments.replace('{{' + key + '}}', form_data[key])
  return command_arguments

if __name__ == "__main__":
    from waitress import serve
    serve(app, host=os.getenv('FLASK_RUN_HOST'), port=os.getenv('FLASK_RUN_PORT'))
